package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport {
	protected static byte sails;
	
	public Ship(String id, byte sails) {
		super(id, 0, 0);
		this.sails = sails;
	}
	
	@Override
	public String move(Road road) {
		return move(road, this);
	}
	public static  String move (Road road, Transport transport) {
		if (road instanceof WaterRoad) 
			return transport.getId() + " " + "Ship" + " is sailing on " + road + " with " + (byte)sails + " sails";
		else
			return "Cannot sail on " + road;
		}
	public static void main(String[] args) {
		Ship ship = new Ship("aaa", (byte) 3);
		System.out.println(ship.move(new WaterRoad("from", "to", 5)));
	}
}
