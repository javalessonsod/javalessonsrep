package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {
	int weight;
	int birthWeight;
	Object stomach;
	
	public Martian() {
		weight = -1;
		}
	
	public Martian (Martian martian) {
		
	}
	@Override
	public void eat(Integer food) {
		
		if (stomach instanceof Integer && (Integer) this.stomach != 0) {
			return;
		}
		
		if (stomach == null) {
			stomach = food;
			weight = weight + (int) stomach;
			
		}
	}
	@Override
	public void eat(Object item) {
		
		if(!(this.stomach == null))
			return;
		
		stomach = item;
	
		if(item instanceof Human) {
			Humanoid tmp = (Humanoid) item;
			weight += tmp.getWeight();
			//stomach = tmp.getWeight();
			tmp.killHimself(); 
		}
		
		if (item instanceof Martian) {
			Martian tmp = (Martian) item;
			weight += tmp.getWeight();
			//tmp.killHimself();
			//stomach = tmp.getWeight();
		}
		if (item instanceof Integer) {
			Integer tmp = (Integer) item;
			weight = weight + tmp;
		}

	}

	@Override
	public Object vomit() {
		// TODO Auto-generated method stub
		
		if (stomach instanceof Integer && (Integer) this.stomach != 0) {
			Integer copyInt = (Integer) this.stomach;
			this.weight -= (Integer) this.stomach;
			
			this.stomach = null;
			return copyInt;
		} else if (stomach instanceof Integer) {
			return this.weight;
		}
		if (stomach == null)
			return null;
		else
			weight = -1; 
		
		Object copy = this.stomach;
		this.stomach = 0;
		
		return copy;
	}
	
	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

	@Override
	public String isAlive() {
		return "I AM IMMORTAL!" ;
	}

	@Override
	public String killHimself() {
		return isAlive();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return this.clone(this);
		}
	
	 private Object clone(Object current) {
		 if (current == null) {
			 return null;
		 }
		 if (current instanceof String) {
			 return new String ((String) current + "");
		 }
				
				if(current instanceof Human) {
					Human currentHuman = (Human) current;
					Human newHuman = new Human();
					newHuman.eat((Integer) currentHuman.vomit());
					return newHuman; 
				}
				if (current instanceof Martian) {
					Martian currentMartian = (Martian) current;
					Martian newMartian = new Martian(((Martian) current));
					newMartian.getWeight();
					return newMartian;
				}
				return current;
		}
		
		
	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + weight + " [" + stomach + "]";
	}
	
	
	public static void main (String[] args) {
		
		Martian m1 = new Martian();
		m1.eat(1);
		System.out.println(m1);
		m1.eat(new Human());
		System.out.println(m1);
		Martian m2 = new Martian();
		Martian m3 = new Martian();
		m1.eat(m2);
		m2.eat(m3);
		System.out.println(m1);
		
	}
}
