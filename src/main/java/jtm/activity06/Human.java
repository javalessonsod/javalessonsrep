package jtm.activity06;

public class Human implements Humanoid {
	int stomach;
	boolean isAlive;
	int weight;
	
	public Human() {
		stomach = 0;
		isAlive = true;
	}
	
	@Override
	public void eat(Integer food) {
		stomach += food;

	}

	@Override
	public Object vomit() {
		int tmp = stomach;
		stomach = 0;
		return tmp;
	}

	@Override
	public String isAlive() {
		if ( isAlive) 
			 return "Alive";
		 else 
			 return "Dead";
	}

	@Override
	public String killHimself() {
		isAlive = false;
		return isAlive();
	}

	@Override
	public int getWeight() {
		return BirthWeight + stomach;
	}
	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + (BirthWeight + stomach) + " [" + stomach + "]";
	}
}
